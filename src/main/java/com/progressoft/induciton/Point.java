package com.progressoft.induciton;

public class Point {

    private int x;
    private int y;

    private static Point staticPoints[][];

    static
    {
        staticPoints = new Point[20][20];

        for(int x = -10; x < 10;x++)
            for(int y = -10; y < 10; y++)
                staticPoints[x+10][y+10] = new Point(x,y);
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public static Point at(int x, int y) {
        if((x >= -10 && x <= 10 && y >= -10 && y <= 10))
            return staticPoints[x+10][y+10];

        return new Point(x,y);
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        //"(" + x + "," + y + ")"
        return sb.append("(").append(Integer.toString(x)).append(",").append(Integer.toString(y)).append(")").toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode()
    {
        return hash();
    }

    private int hash()
    {
        int res = 19;
        res += 3* res + x;
        res += 3* res + y;
        return res;
    }

}

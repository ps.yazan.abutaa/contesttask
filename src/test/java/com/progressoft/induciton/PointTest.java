package com.progressoft.induciton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;

public class PointTest {
    private static Random random = new Random();

    @Test
    public void givenPoint_whenConstruct_thenReturnExpectedValues() {
        int x = random.nextInt();
        int y = random.nextInt();
        Point point = Point.at(x, y);
        Assertions.assertNotNull(point);
        Assertions.assertEquals(x, point.getX());
        Assertions.assertEquals(y, point.getY());
        Assertions.assertEquals("(" + x + "," + y + ")", point.toString());
    }

    @Test
    public void givenTwoPoints_whenCompare_thenReturnExceptedCompareResult() {
        int x = random.nextInt();
        int y = random.nextInt();
        Point p1 = new Point(x, y);
        Point p2 = new Point(x, y);

        Assertions.assertEquals(p1, p2);
        Assertions.assertEquals(p2, p1);
        Assertions.assertEquals(p1.hashCode(), p2.hashCode());
        Assertions.assertEquals(p1, p1);
        Assertions.assertNotEquals(p1, null);
        Assertions.assertNotEquals(p2, null);

        Point p3 = new Point(x + 2, y);
        Assertions.assertNotEquals(p1, p3);
        Assertions.assertNotEquals(p2, p3);
        Assertions.assertNotEquals(p3, p1);

        Point p4 = new Point(x, y + 1);
        Assertions.assertNotEquals(p1, p4);
        Assertions.assertNotEquals(p2, p4);
        Assertions.assertNotEquals(p4, p1);
    }

    @Test
    public void givenPointsCreatedThroughAt_whenComparingReference_thenShouldBeTheSameIfSameCoordinatesFalseOtherwise() {
        //
        //   *                 | 10               *
        //                     |
        //                     |
        //                     |
        //                     |
        // -10------------------------------------- 10
        //                     |
        //                     |
        //                     |
        //                     |
        //                     |
        //   *                -10                  *
        //
        // any points created within square bounded by (-10,-10), (-10,10), (10,10), and (10,-10)
        // should be reused if created through Point.at() method
        testSamePoints(random.nextInt(11), random.nextInt(11));
        testSamePoints(random.nextInt(11), -random.nextInt(11));
        testSamePoints(-random.nextInt(11), random.nextInt(11));
        testSamePoints(-random.nextInt(11), -random.nextInt(11));

        // any points create outside this range, should be a new point
        testNotSamePoints(10 + random.nextInt(11), random.nextInt(11));
        testNotSamePoints(random.nextInt(11), 10 + random.nextInt(11));
        testNotSamePoints(10 + random.nextInt(11), 10 + random.nextInt(11));
        testNotSamePoints(random.nextInt(11), -random.nextInt(11) - 10);
        testNotSamePoints(-random.nextInt(11) - 10, random.nextInt(11));
    }

    private void testSamePoints(int x, int y) {
        Point p1 = Point.at(x, y);
        Assertions.assertNotNull(p1);

        Point p2 = Point.at(x, y);
        Assertions.assertNotNull(p2);

        Assertions.assertTrue(p1 == p2);
        Assertions.assertEquals(p1, p2);
    }

    private void testNotSamePoints(int x, int y) {
        Point p1 = Point.at(x, y);
        Assertions.assertNotNull(p1);

        Point p2 = Point.at(x, y);
        Assertions.assertNotNull(p2);

        Assertions.assertFalse(p1 == p2);
        Assertions.assertEquals(p1, p2);
    }
}
